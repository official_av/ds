namespace DS {
    public class DoublyNode{
        public int data;
        public DoublyNode prev;
        public DoublyNode next;

        public DoublyNode(int data){
            this.data = data;
        }
    }
}