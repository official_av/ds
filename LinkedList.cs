namespace DS{
    public class LinkedList{
        Node head;

        //insert at end of the linked list
        public void append(int data){
            if(head == null){
                head = new Node(data);
                return;
            }
            Node current= head; //start from head
            while(current.next!=null){ //traverse till nth element
                current = current.next;
            }
            current = new Node(data); //insert value at nth element
        }

        //insert at head of the linked list
        public void prepend(int data){
            Node newhead = new Node(data); //create new node
            newhead.next=head; //set current head as next of new node
            head= newhead; //set head to the newly created node
        }

        public void deleteWithValue(int val){
            if(head==null) return; // empty head
            if(head.data==val) { //head is the element to be deleted
                head = head.next;
                return;
            }
            Node current = head;
            while(current.next != null){ // to traverse till end
                // to stop traversal when reach node previous to the one to be deleted
                if(current.next.data==val){
                    // skip the node to be deleted and set pointer to next of the element to be deleted
                    current.next = current.next.next;
                    return;
                }
                // else just traverse to next node
                current = current.next;
            }
        }

        public void insertNodeAtPosition(int data, int position) {
            Node current = head;
            Node newNode = new Node(data);
            if(position == 0){ //for position 0
                newNode.next = head; //set next for new Node as head
                head = newNode; // set head as new Node
                return;
            } else {
                // traverse to position - 1
                for(int i=0;i<position-1;i++) {
                    current=current.next;
                }
                newNode.next = current.next;
                current.next = newNode;
                return;
            }        
    }
    }
    
}
