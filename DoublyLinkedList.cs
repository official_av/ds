namespace DS {
    public class DoublyLinkedList{
        public DoublyNode head;

        public void append(int data){
            if(head == null){ // if head null insert at head
                head = new DoublyNode(data);
                return;
            }
            DoublyNode current = head;
            while(current.next!=null){
                current = current.next; //traverse till last node
            }
            DoublyNode newNode = new DoublyNode(data);
            newNode.prev = current;
            current.next = newNode;
            newNode.next = null;
        }

        public void prepend(int data){
            DoublyNode newNode = new DoublyNode(data);
            newNode.next = head;
            newNode.prev = null;
            head = newNode;
        }

        public void deleteWithValue(int val){
            if(head==null) return; // empty head
            if(head.data==val) { //head is the element to be deleted
                head = head.next;
                head.prev = null;
                return;
            }
            DoublyNode current = head;
            while(current.next!=null){
                if(current.next.data==val) {
                    DoublyNode temp = current.next.next;
                    current.next = temp;
                    temp.prev = current;
                    return;
                }
                current = current.next;
            }            
        }

        public void insertNodeAtPosition(int data, int position){
            DoublyNode newNode = new DoublyNode(data);
            if(position == 0){
                newNode.next = head;
                head.prev = newNode;
                head = newNode;
            } else {                
                DoublyNode current = head;
                for(int i =0;i<position-1;i++){
                    current = current.next;
                }
                DoublyNode temp = current.next;
                newNode.next = temp;
                newNode.prev = current;

                temp.prev = newNode;
                current.next = newNode;
            }
        }
		
		public void print(){
			if(head == null)
				System.Console.WriteLine("empty list");
			else
				{
					DoublyNode current = head;
					while(current!=null)
						{
							System.Console.WriteLine(current.data);
							current=current.next;
						}
				}
		}
        
        public void reverse() {
            DoublyNode current = head;
            if(head == null) return; // empty list
            if(head.next == null) return; //single node list
            while(current!=null){
                // swap prev and next at each node
                DoublyNode temp = current.prev;
                current.prev = current.next;
                current.next = temp;
                // for last node
                if(current.prev==null){
                    head = current;
                }
                // traverse to next node            
                current = current.prev;
            }
            return;       
        }

    }
}